import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// https://medium.com/wdstack/vue-vuex-getting-started-f78c03d9f65
Vue.use(Vuex)

const axiosConfiguration = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': 'application/json, text/plain, */*',
    //timeout: 5000
  }
}

export default new Vuex.Store({
  state: {
    loadingLeitos: true,
    loadingBoletim: true,
    loadingResumo: true,
    leitosUti: null
    /*{
        data: '05/08/2020',
        link: 'https://www.toledo.pr.gov.br/sites/default/files/05-08-2020.png'
      }*/
    ,
    boletimEpidemiologico: null
      /*{
        data: '04/08/2020',
        link: 'https://www.toledo.pr.gov.br/sites/default/files/04-08-2020.png'
      }*/,
    boletimResumo: null
      /*{
        data: '04/08/2020',
        link: 'https://www.toledo.pr.gov.br/sites/default/files/04-08-2020_resumo.png'
      }*/,
    noticias: [
      /*{
        data: '27/07/2020',
        link: 'https://www.toledo.pr.gov.br/noticia/criancas-e-adolescentes-nao-estao-imunes-a-covid-19',
        imagem: 'https://www.toledo.pr.gov.br/sites/default/files/images/crianca_230720_foto_fabio_ulsenheimer_02.jpg',
        titulo: 'CRIANÇAS E ADOLESCENTES NÃO ESTÃO IMUNES À COVID-19',
        desc: 'Médicos da Secretaria de Saúde integrantes do Comitê de Operações Emergenciais (COE) alertaram sobre o número de crianças e adolescentes contagiados com o novo coronavírus.',
        author: 'suzi.lira 27 de julho de 2020 at 14:40h'
      },*/
      /*{
        data: '27/07/2020',
        link: 'https://www.toledo.pr.gov.br/noticia/quatro-ubs-de-toledo-voltarao-a-funcionar-a-partir-da-proxima-segunda-38',
        imagem: 'https://www.toledo.pr.gov.br/sites/default/files/images/ubs_industrial_valderi_geovani_3_0.jpg',
        titulo: 'QUATRO UBS DE TOLEDO VOLTARÃO A FUNCIONAR A PARTIR DA PRÓXIMA SEGUNDA (3/8)',
        desc: 'A Secretaria Municipal de Saúde informa que quatro unidades básicas de saúde (UBS) retomarão os atendimentos a partir da próxima segunda-feira (3/8).',
        author: 'suzi.lira 27 de julho de 2020 at 11:21h'
      },*/
      /*{
        data: '27/07/2020',
        link: 'https://www.toledo.pr.gov.br/noticia/telecorona-movimenta-mais-de-22-mil-ligacoes',
        imagem: 'https://www.toledo.pr.gov.br/sites/default/files/images/tele_-_corona_carlos_rodrigues_01.jpg',
        titulo: 'TELECORONA MOVIMENTA MAIS DE 22 MIL LIGAÇÕES',
        desc: 'Com as novas medidas de restrição adotadas pelo município na época, foi necessário criar um canal de comunicação para que a população pudesse sanar as dúvidas e saber onde e quando procurar atendimento ao apresentar sintomas relacionados ao novo coronavírus.',
        author: 'suzi.lira 23 de julho de 2020 at 16:54h'
      }*/
    ],
    videos: [
      /*{
        data: '23/07/2020',
        codigoVideoYoutube: 'PIVz-uMxxBk',
        titulo: 'Boletim Informativo #Coronavírus (23/07/2020)',
        author: 'suzi.lira 27 de julho de 2020 at 14:40h'
      },
      {
        data: '23/07/2020',
        codigoVideoYoutube: 'hwYLgu9-Ezs',
        titulo: 'Profissionais da linha de frente encaram desafios de saúde',
        author: 'suzi.lira 27 de julho de 2020 at 11:21h'
      },
      {
        data: '23/07/2020',
        codigoVideoYoutube: '54EtMuVwywM',
        titulo: 'Saúde Informa - Visitas em Casa (2/3)',
        author: 'suzi.lira 23 de julho de 2020 at 16:54h'
      }*/
    ]

  },
  actions: {
    loadLeitosUTIData({commit}) {
      axios.post('api/saude/noticias', {"tipo": 1})
          .then((response) => {
            commit('updateLeitosUTI', response.data)
            commit('changeLoadingStateLeitos', false)
          }, (err) => {
            console.log('axios-index-sexto', err)
          })
    },
    loadBoletimData({commit}) {
      axios.post('api/saude/noticias', {"tipo": 2})
            .then((response) => {
              commit('updateBoletim', response.data )
              commit('changeLoadingStateBoletim', false)
            }, (err) => {
              console.log('axios-index-setimo', err)
            })
    },
    loadResumoData({commit}) {
      axios.post('api/saude/noticias', {"tipo": 3})
          .then((response) => {
            commit('updateResumo',  response.data )
            commit('changeLoadingStateResumo', false)
          }, (err) => {
            console.log('axios-index-oitavo', err)
          })
    },
    loadNoticiasData({commit}) {
      axios.post('api/saude/noticias', {"tipo": 4})
          .then((response) => {
            commit('updateNoticias',  response.data )
          }, (err) => {
            console.log('axios-index-nono', err)
          })
    },
    loadVideosData({commit}) {
      axios.post('api/saude/noticias', {"tipo": 5})
          .then((response) => {
            commit('updateVideos',  response.data )
          }, (err) => {
            console.log('axios-index-decimo', err)
          })
    }
  },
  mutations: {
    updateLeitosUTI(state, data) {
      state.leitosUti = data
    },
    updateBoletim(state, data) {
      state.boletimEpidemiologico = data
    },
    updateResumo(state, data) {
      state.boletimResumo = data
    },
    updateNoticias(state, data) {
      state.noticias = data
    },
    updateVideos(state, data) {
      state.videos = data
    },
    changeLoadingStateLeitos(state, loading) {
      state.loadingLeitos = loading
    },
    changeLoadingStateBoletim(state, loading) {
      state.loadingBoletim = loading
    },
    changeLoadingStateResumo(state, loading) {
      state.loadingResumo = loading
    }
  },
  modules: {
  }
})
