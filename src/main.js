import Vue from 'vue'
import App from './App.vue'
// import './registerServiceWorker'
import router from './router'
import store from './store'
import './plugins/axios'

import VueResizeText from 'vue-resize-text'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faTwitter, faInstagram, faFacebook, faFacebookF } from '@fortawesome/free-brands-svg-icons'
//import './plugins/icon';
//import { far } from '@fortawesome/pro-regular-svg-icons'
//import { fas } from '@fortawesome/pro-solid-svg-icons'
//import { fal } from '@fortawesome/pro-light-svg-icons'
//import { fab } from '@fortawesome/free-brands-svg-icons'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@/assets/styles/aquamarine.css'

//library.add(faUserSecret)
library.add(faTwitter)
library.add(faInstagram)
library.add(faFacebook)
library.add(faFacebookF)
//library.add(far);
//library.add(fas);
//library.add(fal);
//library.add(fab);

Vue.use(VueResizeText)
Vue.component(FontAwesomeIcon.name, FontAwesomeIcon)

Vue.config.productionTip = false

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)


new Vue({
  router,
  store,
  mounted() {
    //console.log('Mounted')
    //console.log(this.$store)
    this.$store.dispatch('loadLeitosUTIData') // dispatch loading
    this.$store.dispatch('loadBoletimData')
    this.$store.dispatch('loadResumoData')
    this.$store.dispatch('loadNoticiasData')
    this.$store.dispatch('loadVideosData')
  },
  render: h => h(App)
}).$mount('#app')
