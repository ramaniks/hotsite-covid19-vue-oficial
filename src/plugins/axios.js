import Vue from 'vue'
import axios from 'axios'

//Vue.http.headers.common['Access-Control-Allow-Origin'] = 'https://app.toledo.pr.gov.br'
//Vue.http.headers.common['Access-Control-Request-Method'] = '*'

//axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
//axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
//axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
//axios.defaults.headers.common['Authorization'] = 'Basic YWxhZGRpbjpvcGVuc2VzYW1l'

axios.defaults.baseURL = 'https://app.toledo.pr.gov.br/'
//axios.defaults.baseURL = 'http://pmtoledooficial.too'

axios.defaults.xsrfHeaderName = null //'X-CSRFToken'
axios.defaults.headers.get['Pragma'] = 'no-cache'
axios.defaults.headers.get['Cache-Control'] = 'no-cache, no-store'
axios.defaults.headers.get['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.headers.get['Accept'] = 'application/json,text/plain,*/*'
//axios.defaults.timeout =  1000

Vue.use({
    install(Vue) {
        Vue.prototype.$http = axios
        /*Vue.prototype.$http = axios.create({
            baseUrl: `http://localhost:3000/`,
            url: 'http://localhost:3000/'
        })*/
    }
})