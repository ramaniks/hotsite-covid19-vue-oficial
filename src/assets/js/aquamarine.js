/**
 * navbar-ontop.js 1.0.0
 * Add .navbar-ontop class to navbar when the page is scrolled to top
 * Make sure to add this script to the <head> of page to avoid flickering on load
 */

(function() {

    var className = "navbar-ontop"

    // we start hidden, to avoid flickering
    document.write("<style id='temp-navbar-ontop'>.navbar {opacity:0; transition: none !important}</style>")

    function update() {
        // toggle className based on the scrollTop property of document
        var nav = document.querySelector(".navbar")

        if (window.scrollY > 15)
            nav.classList.remove(className)
        else
            nav.classList.add(className)
    }

    document.addEventListener("DOMContentLoaded", function(event) {
        $(window).on('show.bs.collapse', function (e) {
            $(e.target).closest("." + className).removeClass(className);
        })

        $(window).on('hidden.bs.collapse', function (e) {
            update()
        })
        update()
        // still hacking to avoid flickering
        setTimeout(function() {
            document.querySelector("#temp-navbar-ontop").remove()
        })
    });

    window.addEventListener("scroll", function() {
        update()
    })

})();

/**
 * smooth-scroll.js 1.0.0
 * Make your page scrolling smooth
 * Requires JQuery - Does not work with JQuery slim
 * Based on https://css-tricks.com/snippets/jquery/smooth-scrolling/
 */

(function() {

    var duration = 500

    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .click(function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']')
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault()

                    $('html, body').animate({
                            scrollTop: target.offset().top
                        },
                        duration, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                }
            }
        });

})();

/**
 * animate-in.js 1.0.0
 * Animate elements on entrance
 *
 * Usage:
 *
 * Make sure to add this to the <head> of your page to avoid flickering on load
 * Based on https://andycaygill.github.io/scroll-entrance/
 */
(function() {

    //Set up defaults
    var duration = "2000";
    var heightOffset = 100;


    // document.write("<style id='temp-animate-in'>*[class^='animate-in'], *[class*=' animate-in'] {display:none}</style>")

    function isElementVisible(elem) {

        var rect = elem.getBoundingClientRect();

        //Return true if any of the following conditions are met:
        return (
            // The top is in view: the top is more than 0 and less than the window height (the top of the element is in view)
            ( (rect.top + heightOffset) >= 0 && (rect.top + heightOffset) <= window.innerHeight ) ||
            // The bottom is in view: bottom position is greater than 0 and greater than the window height
            ( (rect.bottom + heightOffset) >= 0 && (rect.bottom + heightOffset) <= window.innerHeight ) ||
            // The top is above the viewport and the bottom is below the viewport
            ( (rect.top + heightOffset) < 0 && (rect.bottom + heightOffset) > window.innerHeight )
        )

    }


    function update() {
        var nodes = document.querySelectorAll("*:not(.animate-in-done)[class^='animate-in'], *:not(.animate-in-done)[class*=' animate-in']")

        for (var i = 0; i < nodes.length; i++) {
            if (isElementVisible(nodes[i])) {
                nodes[i].classList.remove("out-of-viewport")
                nodes[i].classList.add("animate-in-done")
            } else {
                nodes[i].classList.add("out-of-viewport")
            }
        }
    }

    document.addEventListener("DOMContentLoaded", function(event) {
        update()
        // setTimeout(function() {
        //   document.querySelector("#temp-animate-in").remove()
        // })
    });

    window.addEventListener("scroll", function() {
        update()
    })

})();

