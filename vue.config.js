module.exports = {
    lintOnSave: false,
    publicPath: process.env.NODE_ENV === 'production'
        ? '/covid19/'
        : '/',
    assetsDir: undefined,
    outputDir: 'dist',
    /* https://github.com/vuejs/vue-cli/issues/1649 */
    /* https://github.com/TAGC/vue-expandable-grid/blob/5a3f4562d5151989a71311ba5f16b4d0949a2bc4/vue.config.js#L1-L16 */
    chainWebpack: config => {
        if(config.plugins.has('extract-css')) {
            const extractCSSPlugin = config.plugin('extract-css')
            extractCSSPlugin && extractCSSPlugin.tap(() => [{
                filename: '[name].css',
                chunkFilename: '[name].css'
            }])
        }
    },
    /* configureWebpack: {
        output: {
            filename: '[name].js',
            chunkFilename: '[name].js'
        }
    }  ,
    devServer: {
        proxy: {
            '/api': {
                target: 'http://pmtoledooficial.too',
                pathRewrite: {'^/api' : ''}
            },
            '/saude/api': {
                target: 'http://pmtoledooficial.too',
                pathRewrite: {'^/saude/api' : ''}
            }
        }
    }*/
  }